
'use strict';

/* Controllers */

var appList = angular.module('appList', []);

appList.controller('AppListCtrl', /* function ($scope) {
  $scope.apps = [
  	{"name": "app1",
  	},
  	{"name": "app2",
  	},
  	{"name": "app3",
  	}
  ];
*/

function ($scope , $http) {
  $http.get('http://192.168.1.100:8080/jobs').success(function(data) {
    $scope.apps = data;
  });
});

appList.controller('NewAppCtrl', function ($scope, $http) {
  $scope.master= {};

  $scope.update = function(app) {
    $scope.master= angular.copy(app);
    $http.post('http://192.168.1.100:8080/jobs', app);
  };

  $scope.reset = function() {
    $scope.app = angular.copy($scope.master);
  };

  $scope.reset();
});


