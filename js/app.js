var restify = require('restify');

 
var ip_addr = '127.0.0.1';
var port    =  '8080';
 
var server = restify.createServer({
    name : "myapp"
});
 
server.use(restify.queryParser());
server.use(restify.bodyParser());
server.use(restify.CORS());


server.listen(port ,ip_addr, function(){
    console.log('%s listening at %s ', server.name , server.url);
});

var PATH = '/jobs'

server.get(/^\/((.*)(\.)(.+))*$/, restify.serveStatic({ directory: 'public', default: "index.html" }));  

server.get({path : PATH , version : '0.0.1'} , findAllJobs);
server.get({path : PATH +'/:jobId' , version : '0.0.1'} , findJob);
server.post({path : PATH , version: '0.0.1'} ,postNewJob);
server.del({path : PATH +'/:jobId' , version: '0.0.1'} ,deleteJob);

function findAllJobs(req, res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    success = true
    err = 'error'
   // result = { name: 'jade', time: '1975-11-01', id: 1, type : {bisness:1, noprofit: true}};
    result = {name :'jade'};
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(200 , result);
            return next();
        }else{
            return next(err);
        }
 
}
 
function findJob(req, res , next){
    res.setHeader('Access-Control-Allow-Origin','*');

    success = true
    err = 'error'
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(200 , success);
            return next();
        }
        return next(err);

}
 
function postNewJob(req , res , next){
    var job = {};
    console.log('job json content:'+JSON.stringify(req.params));
    job.title = req.params.title;
   /* job.description = req.params.description;
    job.location = req.params.location;
    job.postedOn = new Date();
 */
    res.setHeader('Access-Control-Allow-Origin','*');
 
    success = true
    err = 'error'
        console.log('job json content:'+JSON.stringify(job));
     //   console.log('Response error '+err);
        if(success){
            res.send(201 , job);
            return next();
        }else{
            return next(err);
        }

}
 
function deleteJob(req , res , next){
    res.setHeader('Access-Control-Allow-Origin','*');
    
    success = true
    err = 'error'
        console.log('Response success '+success);
        console.log('Response error '+err);
        if(success){
            res.send(204);
            return next();      
        } else{
            return next(err);
        }
 
}


