package com.qcy.task;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.FileTransferNegotiator;
import org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer;
import org.jivesoftware.smackx.filetransfer.FileTransfer.Status;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.Occupant;

import android.util.Log;

import com.qcy.constant.Constant;

import csdn.shimiso.eim.manager.XmppConnectionManager;

public class RoomFileTransferTask extends Thread {
	
	String fileName;
	MultiUserChat muc;
	String userName;
	
	public RoomFileTransferTask(String fileName,MultiUserChat muc,String userName){
		this.fileName = fileName;
		this.muc = muc;
		this.userName = userName;
	}

	public void run() {
		try {
			sendAudioChatMessage(Constant.MESSAGE_TYPE_AUDIO,
					fileName, getRoomMembersJID(),
					XmppConnectionManager.getInstance().getConnection());
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * @author shdyuali
	 * @param messageContent
	 * @throws Exception
	 */
	protected void sendAudioChatMessage(int type, String fileName,
			ArrayList<String> toJIDs, XMPPConnection connection)
			throws Exception {

		FileTransferNegotiator.setServiceEnabled(connection, true);

		FileTransferManager manager = new FileTransferManager(connection);

		for (String al : toJIDs) {
			// �ų��Լ�
			if (al.startsWith(userName + "@")) {
				continue;
			}
			Log.e("MultiRoomActivity",al);
			OutgoingFileTransfer transfer = manager
					.createOutgoingFileTransfer(al);
			
			Log.e("MultiRoomActivity",fileName);
			
			try {
				transfer.sendFile(new File(fileName), "Audio");
			} catch (XMPPException e) {
				e.printStackTrace();
			}
			while (!transfer.isDone()) {
				if (transfer.getStatus().equals(Status.error)) {
					System.out.println("ERROR!!! " + transfer.getError());
				} else if (transfer.getStatus().equals(Status.cancelled)
						|| transfer.getStatus().equals(Status.refused)) {
					System.out.println("Cancelled!!! " + transfer.getError());
				}
				try {
					Thread.sleep(1000L);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			if (transfer.getStatus().equals(Status.refused)
					|| transfer.getStatus().equals(Status.error)
					|| transfer.getStatus().equals(Status.cancelled)) {
				System.out.println("refused cancelled error "
						+ transfer.getError());
			} else {
				System.out.println("Success");
			}
		}

		muc.sendMessage(fileName);
	}
	
	public ArrayList<String> getRoomMembersJID() {
		ArrayList<String> alTemp = new ArrayList<String>();
		
		Iterator<String> iterator = muc.getOccupants();
		
		while(iterator.hasNext()){
			String occStr = iterator.next();
			Occupant occ = muc.getOccupant(occStr);
			String jid = occ.getJid();
			alTemp.add(jid);
		}
		
		Log.e("RoomMembersJID",alTemp.toString());
		return alTemp;
	}
}
