package com.qcy.chatmessage;

import java.util.Date;

import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.packet.DelayInformation;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;

import com.qcy.constant.Constant;

public class ChatPacketListener implements PacketListener {
	private Handler handler;
	
	public ChatPacketListener(Handler handler) {
		this.handler = handler;
	}

	@Override
	public void processPacket(Packet packet) {
		Message message = (Message) packet;
		String from = message.getFrom();

		if (message.getBody() != null) {
			DelayInformation inf = (DelayInformation) message.getExtension(
					"x", "jabber:x:delay");
			Date sentDate;
			if (inf != null) {
				sentDate = inf.getStamp();
			} else {
				sentDate = new Date();
			}

			Log.w("Chat",
					"Receive old message: date="
							+ sentDate.toLocaleString() + " ; message="
							+ message.getBody());

			android.os.Message msg = new android.os.Message();
			msg.what = Constant.RECEIVE;
			Bundle bd = new Bundle();
			bd.putString("from", from);
			bd.putString("body", message.getBody());
			bd.putString("time", sentDate.toLocaleString());
			msg.setData(bd);
			handler.sendMessage(msg);
		}
	}
}