/**
 * copyright qicayun
 */
package com.qcy.chatmessage;

import java.util.List;

import org.jivesoftware.smack.XMPPException;

import android.widget.ListView;

import com.qcy.adapter.AMessageAdapter;
import com.qcy.model.IMMessage;

/**
 * 实现点对点文本和语音聊天，聊天室文本和语音群聊 
 * @author shdyuali
 * 
 */
public interface IChatSupport {


	/**
	 * 播放语音文件
	 * @param fileName 语音文件地址，绝对路径
	 */
	public void playAudio(String fileName);
	
	/**
	 * 开始录音，保存在mark指定的文件夹下
	 * @param mark 文件夹
	 */
	public void startRecord(String mark);
	
	/**
	 * 停止录音
	 * @return 录音文件的地址，绝对路径
	 */
	public String stopRecord();
	
	/**
	 * 聊天初始化
	 * @param toJID 聊天对象JID
	 * @param adapter 聊天历史列表适配器
	 * @throws XMPPException
	 */
	public abstract void initChat(String toJID, AMessageAdapter adapter)throws XMPPException;

	/**
	 * 发送文本消息
	 * @param messageContent 文本消息
	 * @param listview 显示文本消息的列表对象
	 * @throws XMPPException
	 */
	public abstract void sendTxtMessage(String messageContent)throws XMPPException;

	/**
	 * 发送语音留言消息
	 * @param filePath 语音文件地址，绝对路径
	 */
	public abstract void sendVoiceMessage(String filePath);
	
	/**
	 * 初始化群聊聊天室
	 * @param roomJid 聊天室JID
	 * @param userNickName 加入聊天室的用户别名
	 * @param roomMsgItems 群聊消息集合
	 * @param roomAdapter 聊天列表适配器
	 */
	public abstract void initMultiRoom(String roomJid, String userNickName,List<IMMessage> roomMsgItems,AMessageAdapter roomAdapter);
	
	/**
	 * 向聊天室发送文本消息
	 * @param message 文本消息
	 * @throws XMPPException
	 */
	public abstract void sendRoomTxtMessage(String message) throws XMPPException;
	
	/**
	 * 向聊天室发送语音留言消息
	 * @param fileName 语音文件地址， 绝对路径
	 * @param userName 加入聊天室的用户名
	 */
	public void sendRoomVoiceMessage(String fileName,String userName);
	
	/**
	 * 
	 */
	public abstract void sendImageMessage();
	
	/**
	 * 
	 */
	public abstract void sendRoomImageMessage();
}
