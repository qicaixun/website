package com.qcy.chatmessage;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.provider.ProviderManager;
import org.jivesoftware.smackx.bytestreams.socks5.provider.BytestreamsProvider;
import org.jivesoftware.smackx.filetransfer.FileTransfer.Status;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.FileTransferNegotiator;
import org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.Occupant;
import org.jivesoftware.smackx.provider.DiscoverInfoProvider;
import org.jivesoftware.smackx.provider.DiscoverItemsProvider;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.qcy.adapter.AMessageAdapter;
import com.qcy.constant.Constant;
import com.qcy.manager.MessageManager;
import com.qcy.model.IMMessage;
import com.qcy.service.IMChatService;
import com.qcy.task.RoomFileTransferTask;
import com.qcy.util.AudioRecorderUtil;

import csdn.shimiso.eim.manager.XmppConnectionManager;
import csdn.shimiso.eim.util.DateUtil;

public class ChatMessage implements IChatSupport {
	
	private final String CLASSNAME = this.getClass().getName();
	private Context context = null;
	private Chat chat = null;
	private XMPPConnection connection;
	private List<IMMessage> message_pool = null;
	private AudioRecorderUtil recorder;
	private AMessageAdapter adapter;
	private MultiUserChat muc;
	private List<IMMessage> roomMsgItems;
	private AMessageAdapter roomAdapter;
	
	/**
	 * 构造函数
	 * @param context 上下文，activity页面
	 */
	public ChatMessage(Context context){
		recorder = new AudioRecorderUtil();
		this.context = context;
		
		ProviderManager.getInstance().addIQProvider("query",
        "http://jabber.org/protocol/bytestreams",
        new BytestreamsProvider());
ProviderManager.getInstance().addIQProvider("query",
        "http://jabber.org/protocol/disco#items",
        new DiscoverItemsProvider());
ProviderManager.getInstance().addIQProvider("query",
        "http://jabber.org/protocol/disco#info",
        new DiscoverInfoProvider());
		
		// 聊天服务
		Intent chatServer = new Intent(context, IMChatService.class);
		context.startService(chatServer);
	}	
	
	public List<IMMessage> getMessages() {
		return message_pool;
	}

	@Override
	public void initChat(String toJID,AMessageAdapter adapter) throws XMPPException {
		if(adapter!=null){
			this.adapter = adapter;
		}
			
		if(toJID != null||"".equals(toJID)){
			chat = XmppConnectionManager.getInstance().getConnection().getChatManager().createChat(toJID, null);
		}else{
			Toast.makeText(context, "聊天者不能为空",	Toast.LENGTH_SHORT).show();
		}
		
		// 第一次查询
		message_pool = MessageManager.getInstance(context)
				.getMessageListByFrom(toJID, 1, 10);
		if (null != message_pool && message_pool.size() > 0)
			Collections.sort(message_pool);
	}



	@Override
	public void sendTxtMessage(String messageContent)throws XMPPException {
		if (messageContent==null||"".equals(messageContent)||context==null){
			Toast.makeText(context, "参数不能为空",	Toast.LENGTH_SHORT).show();
			return;
		}
		

		String time = DateUtil.date2Str(Calendar.getInstance(),	Constant.MS_FORMART);
		Message message = new Message();
		message.setProperty(IMMessage.KEY_TIME, time);
		message.setBody(messageContent);
		chat.sendMessage(message);
		
		IMMessage newMessage = new IMMessage();
		newMessage.setMsgType(1);
		newMessage.setFromSubJid(chat.getParticipant());
		newMessage.setContent(messageContent);
		newMessage.setTime(time);
		message_pool.add(newMessage);
		MessageManager.getInstance(context).saveIMMessage(newMessage);

		// 刷新视图
		refreshMessage(message_pool,adapter);		
	}
	
	private void refreshMessage(List<IMMessage> messages,AMessageAdapter adapter) {
//		adapter.setItems(messages);
////		((BaseAdapter)(listview.getAdapter())).notifyDataSetChanged();
//		adapter.notifyDataSetChanged();
//		listview.setSelection(messages.size() - 1);
		adapter.refreshList(messages);
	}
	
	
	public void startRecord(String mark){
		if(mark != null && !"".equals(mark)){
			recorder.startRecord(mark);
		}
	}
	
	public String stopRecord(){
		if(context != null){
			return recorder.stopRecord(context);
		}
		return "";
	}
	
	@Override
	public void playAudio(String fileName) {
		if (fileName != null
				&& fileName.endsWith(".3gp")
				&& fileName.endsWith(".arm")) {			
			recorder.playAudio(fileName);
		}		
	}
	
	
	@Override
	public void sendVoiceMessage(String filePath) {
		connection = XmppConnectionManager.getInstance().getConnection();
		FileTransferNegotiator.setServiceEnabled(connection, true);
		
		FileTransferManager manager = new FileTransferManager(connection);
		
		String toJID = chat.getParticipant();
		OutgoingFileTransfer transfer = manager.createOutgoingFileTransfer(toJID+"/Smack");
		
		try {
				transfer.sendFile(new File(filePath), "Audio");
			} catch (XMPPException e) {
			   e.printStackTrace();
			}
			while(!transfer.isDone()) {
			   if(transfer.getStatus().equals(Status.error)) {
			      System.out.println("ERROR!!! " + transfer.getError());
			   } else if (transfer.getStatus().equals(Status.cancelled)
			                    || transfer.getStatus().equals(Status.refused)) {
			      System.out.println("Cancelled!!! " + transfer.getError());
			   }
			   try {
			      Thread.sleep(1000L);
			   } catch (InterruptedException e) {
			      e.printStackTrace();
			   }
			}
			if(transfer.getStatus().equals(Status.refused) || transfer.getStatus().equals(Status.error)
			 || transfer.getStatus().equals(Status.cancelled)){
			   System.out.println("refused cancelled error " + transfer.getError());
			} else {
			   System.out.println("Success");
			}

		
		String time = DateUtil.date2Str(Calendar.getInstance(),
				Constant.S_FORMART);

		IMMessage newMessage = new IMMessage();
		newMessage.setMsgType(1);
		newMessage.setFromSubJid(toJID);
		newMessage.setContent(filePath);
		newMessage.setTime(time);
		message_pool.add(newMessage);
		MessageManager.getInstance(context).saveIMMessage(newMessage);

		// 刷新视图
		refreshMessage(message_pool,adapter);
	}

	@Override
	public void sendImageMessage() {
		// TODO Auto-generated method stub
	}

	@Override
	public void initMultiRoom(String roomJid, String userNickName,List<IMMessage> roomMsgItems,AMessageAdapter roomAdapter) {
		if(roomMsgItems!=null &&roomJid!=null&& !"".equals(roomJid)&& roomAdapter!=null){
			this.roomMsgItems = roomMsgItems;
			this.roomAdapter = roomAdapter;
		}else{
			Log.e(CLASSNAME,"参数不满足要求");
		}
		
		// 后面服务名称必需是创建房间的那个服务
		try {
			muc = new MultiUserChat(XmppConnectionManager.getInstance()
					.getConnection(), roomJid);
			// 创建聊天室,进入房间后的nickname
			muc.join(userNickName);

		} catch (XMPPException e) {
			e.printStackTrace();
			
		}				
		
		ChatPacketListener chatListener = new ChatPacketListener(handler);
		muc.addMessageListener(chatListener);
		
		// 第一次查询
//		this.roomMsgItems = MessageManager.getInstance(context)
//				.getMessageListByRoom(userNickName, 1, 10,roomJid);
//		if (null != roomMsgItems && roomMsgItems.size() > 0)
//			Collections.sort(roomMsgItems);
		
	}
	
	@Override
	public void sendRoomTxtMessage(String message) throws XMPPException{
		muc.sendMessage(message);
	}
	
	public Handler handler = new Handler() {

		@Override
		public void handleMessage(android.os.Message msg) {

			switch (msg.what) {
			case Constant.RECEIVE: {
				Bundle bundle = msg.getData();
				receiveMsg(bundle);
			}
				break;
			default:
				break;
			}
		}
	};
	
	private void receiveMsg(Bundle bd) {
		IMMessage im = new IMMessage();
		im.setFromSubJid(bd.getString("from"));
		im.setMsgType(0);
		im.setContent(bd.getString("body"));
		im.setTime(bd.getString("time"));
		roomMsgItems.add(im);

		// 刷新视图
		refreshMessage(roomMsgItems,roomAdapter);
	}

	@Override
	public void sendRoomVoiceMessage(String fileName,String userName) {
		
		RoomFileTransferTask roomThread = new RoomFileTransferTask(fileName,muc,userName);
		roomThread.start();
	}
	
	public String getUserNick(String jid){
		String userNick = "";
		if(jid!=null && !"".equals(jid)){
			Occupant occ = muc.getOccupant(jid);
			userNick = occ==null?jid:occ.getNick();
		}
			return userNick;
	}
	
	public ArrayList<String> getRoomMembersNick() {
		ArrayList<String> alTemp = new ArrayList<String>();
				
		Iterator<String> iterator = muc.getOccupants();
		
		while(iterator.hasNext()){
			String occStr = iterator.next();
			Occupant occ = muc.getOccupant(occStr);
			String nickName = occ.getNick();
			alTemp.add(nickName);
		}
		
		
		Log.e("RoomMembersNick",alTemp.toString());
		return alTemp;
	}

	@Override
	public void sendRoomImageMessage() {
		// TODO Auto-generated method stub
	}

}
