package com.qcy.util;

import java.io.File;
import java.io.IOException;

import com.qcy.constant.Constant;

import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;
import csdn.shimiso.eim.util.FileUtil;

/**
 * 这是录音，播放录音的工具类
 * @author hu
 *
 */
public class AudioRecorderUtil {
	private static final String LOG_TAG="AudioRecorderUtil";
	private MediaRecorder mRecorder;
	private MediaPlayer mPlayer;
    private String fileName;
//    private Long time1;
//    private Long time2;
	
	
	public void startRecord(String mark){
		File file=FileUtil.createFile(mark, Constant.MESSAGE_TYPE_AUDIO);
		fileName=file.getAbsolutePath();
		Log.e(LOG_TAG, "fileName:"+fileName);
		
		mRecorder = new MediaRecorder();
        mRecorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        mRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);
        mRecorder.setOutputFile(fileName);
        mRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);

        try {
            mRecorder.prepare();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed--"+e.toString());
        }

        mRecorder.start();
//        time1 = System.currentTimeMillis();
//        Log.e(LOG_TAG, "time1:"+time1);
    }
	
	public String stopRecord(Context context){
//		time2 = System.currentTimeMillis();
//		Log.e(LOG_TAG, "time2:"+time2);
//		long dur = time2-time1;
//		Log.e(LOG_TAG, "dur:"+dur);
//		if(dur<10){
//			Toast.makeText(context, "说话时间太短!", Toast.LENGTH_SHORT).show();
//			mRecorder.reset();
//			mRecorder=null;
//			return "";
//		}
		
		if(mRecorder!=null){
//			Log.e(LOG_TAG, "~~~~~mRecorder is not null~~~~");
			mRecorder.stop();
			mRecorder.release();
			mRecorder=null;
		}else{
			Log.e(LOG_TAG, "~~~~~mRecorder is null~~~~");
		}
		return fileName;
	}
	
	public void playAudio(String fileName){
		mPlayer = new MediaPlayer();
        try {
            mPlayer.setDataSource(fileName);
            mPlayer.prepare();
            mPlayer.start();
        } catch (IOException e) {
            Log.e(LOG_TAG, "prepare() failed");
        }
	}
	
	/*
	public void playVoiceMsg(String path){
		if (path != null
				&& path.endsWith(".3gp")
				&& path.startsWith(Environment
						.getExternalStorageDirectory().toString())) {

			// 设置控制音乐声音
			setVolumeControlStream(AudioManager.STREAM_MUSIC);
			// 创建MediaPlayer对象
			MediaPlayer mPlayer = new MediaPlayer();

			try {
				mPlayer.setDataSource(path);
				mPlayer.prepare();
			} catch (Exception e) {
				e.printStackTrace();
			}
			// 开发播放音乐
			mPlayer.start();
			while (mPlayer.isPlaying()) {
				Log.e("ChatActivity", "mediaplayer is playing!");
				try {
					Thread.sleep(1000L);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

			}
		}
	}
	*/
}
