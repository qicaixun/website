package com.qcy.adapter;

import java.util.List;

import android.widget.BaseAdapter;
import android.widget.ListView;

import com.qcy.model.IMMessage;

public abstract class AMessageAdapter extends BaseAdapter {

	protected List<IMMessage> items;
//	private Context context;
	protected ListView listView;

	public AMessageAdapter(List<IMMessage> items,
			ListView listView) {
		this.items = items;
		this.listView = listView;
	}
	
//	public Context getContext(){
//		return context;
//	}
//	
//	public void setContext(Context context){
//		this.context = context;
//	}

	public void refreshList(List<IMMessage> items) {
		this.items = items;
		this.notifyDataSetChanged();
		listView.setSelection(items.size() - 1);
	}

	@Override
	public int getCount() {
		return items == null ? 0 : items.size();
	}
	
	public void setItems(List<IMMessage> items){
		this.items = items;
	}

	@Override
	public Object getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}


}